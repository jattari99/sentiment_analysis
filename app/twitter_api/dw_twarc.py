from twarc.client2 import Twarc2
from twarc.expansions import flatten

max_tweets = 1000
since = datetime.datetime(2021, 6, 5, 0, 0, tzinfo=datetime.timezone.utc)
until = datetime.datetime(2021, 6, 8, 0, 0, tzinfo=datetime.timezone.utc)
search_results = t.search_all(query= "(fendi) lang:it", start_time=since, end_time=until, max_results=500)
numpages = math.ceil(max_tweets / 500)

all_tweets = []
for page in itertools.islice(search_results, numpages):
    for tweet in flatten(page)['data']:
         all_tweets.append(parsetw(tweet))
    print("Fetched", len(all_tweets), "tweets")