import os
import tweepy as tw
import pandas as pd
import datetime
from decouple import config


consumer_key = config("API_KEY")
consumer_secret = config("API_SECRET_KEY")
access_token = config("API_ACCESS_TOKEN")
access_token_secret = config("API_ACCESS_TOKEN_SECRET")


auth = tw.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tw.API(auth, wait_on_rate_limit=True)

search_words = [
    "election22",
    "python",
]


startDate = datetime.datetime(2022, 1, 1, 0, 0, 0)
endDate = datetime.datetime(2022, 4, 20, 0, 0, 0)
# Collect tweets

tweets = tw.Cursor(
    api.search_tweets,  # ('DEV',search_words[2]),
    q="#election22",
    until="2022-04-20",
    lang="en",
).items(5000)


# tweets =tw.Cursor(api.search_all_tweets('DEV',search_words[2],fromDate='202201010000',toDate='202201230000')).pages()
# Iterate and print tweets
i = 0
for tweet in tweets:
    i += 1
print(i)


twarc2 search --archive --start-time 2022-01-01 --end-time 2022-01-24 --limit 1 "Djokovic OR Novak Djokovic OR #novakdjokovic OR#djokovic OR djokovicsaga" results.jsonl
