# Sentiment Analysis

## twarc2

* Example for mr Djokovic
```bash
twarc2 search --archive --start-time 2022-01-01 --end-time 2022-01-24 --limit 50 "Djokovic OR Novak Djokovic OR #novakdjokovic OR #djokovic OR djokovicsaga" results.json
```

* Example for mr Bojic
```bash
twarc2 search --archive --start-time 2022-01-01 --end-time 2022-04-15 --limit 50 "Ljubisa Bojic" results.json

```

* Check my likes
  * Radovan: 958348593605292038
  * Ljubisa: 3227817851
```bash
twarc2 liked-tweets {twitter_id} results.json
```

twarc2 liked-tweets --limit 20 3227817851 20 results.json

## Getting started
